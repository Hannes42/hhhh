var steps = {};

// die knöpfe sind jeweils per hand im dict, damit man volle freiheit hat, was geht. man könnte ja auch branchen o.ä.
// die step namen können frei gewählt werden, sind hier einfach numerisch, aber alles geht
// text in backticks kann in javascript über mehrere zeilen gehen etc \o/

steps["intro"] = {
	"mapID": 15832,
	"lat": 53.548681,
	"lon": 9.979588,
	"zoomlevel": 17,
	"description": `<h3>Der Michel</h3>
	<p>Der Michel oberhalb des Hafens ist Hamburgs Hauptkirche und ein Wahrzeichen der Stadt. Er ist einer der bedeutendsten Kirchenbauten des Barocks in Norddeutschland. Das Kirchenspiel St. Michaelis entstand zu Anfang des 17. Jahrhunderts mit der Neustadt.</p>
	<br />
	<div id='navigation'><button onclick='displayStep("2")'>→</button></div>`,
};

steps["2"] = {
	"description": `Die erste 1648-61 erbaute Kirche und ihr 1669 vollendeter Turm vernichtete 1750 ein vom Blitz entfachtes Feuer.
	<a href="https://resolver.sub.uni-hamburg.de/goobi/PPN66986451X" target="_blank"><img class='fittedimage' src='images/thumbnail/66986451X.jpg' /></a>
	<br /><br /><br />
	<div id='navigation'>
	<button onclick='displayStep("intro")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("3")'>→</button>
	</div>`,
};

steps["3"] = {
	"description": `<a href="https://resolver.sub.uni-hamburg.de/goobi/PPN751414379" target="_blank"><img class='fittedimage' src='images/thumbnail/PPN751414379.jpg' /></a>
	<div id='navigation'>
	<button onclick='displayStep("2")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("4")'>→</button>
	</div>`,
};

steps["4"] = {
	"mapID": 15841,
	"zoomlevel": 17,
	"description": `Der zweite Bau wurde nach Plänen von E. G. Sonnin und J.L. Prey in den Jahren 1750-62 als Saalbau in Form eines Griechischen Kreuzes errichtet. Den 132 m hohen Turm ergänzte Sonnin in den Jahren 1777-85.
	<a href="https://resolver.sub.uni-hamburg.de/goobi/PPN75141347X" target="_blank"><img class='fittedimage' src='images/thumbnail/PPN75141347X.jpg' /></a>
	Dieser Bau wurde 1906 durch einen bei Reparaturarbeiten verursachten Brand fast vollständig zerstört.
	<br /><br /><br />
	<div id='navigation'>
	<button onclick='displayStep("3")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("5")'>→</button>
	</div>`,
};

steps["5"] = {
	"description": `
	<p>Der Neubau 1907-12 ist aber weitgehend mit dem Vorgänger identisch. Bemerkenswert an dem 3.000 Menschen fassenden, in Weiß und Gold gehaltenen Innenraum sind der barocke Opferstock von 1763 und der 20m hohe Altar (1912).</p>
	<p>Die Fassade zieren eine Statue des hl. Michael mit dem Drachen (über dem Turmportal, 1912), an der Nordseite ein Bronzestandbild Luthers (1912, O. Lessing) und eine Büste von Bürgermeister J.H. Burchard (1912) sowie an der Südseite ein Gedenkrelief für Sonnin, der auch im Michel begraben liegt.</p>
	<p>Im Turm, 82m über der Straße befindet sich eine offene, für Besucher zugängliche Säulenhalle. Nach Beseitigung von Kriegsschäden (G. Langmaack) wurde St. Michaelis 1952 neu geweiht. Seit 1983 wurde die Kirche aufwendig renoviert; 1996 konnte die Turmerneuerung abgeschlossen werden.</p>
	<br />
	<div id='navigation'>
	<button onclick='displayStep("4")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("6")'>→</button>
	</div>`,
};

steps["6"] = {
	"description": `
	<p>Quelle für den Text: Kopitzsch, Franklin und Tilgner, Daniel (Hrsg.), 1998: Hamburg-Lexikon. Hamburg: Zeise. ISBN 3-9805687-9-2.</p>
	<br />
	<div id='navigation'><button onclick='displayStep("intro")'>⌂</button></div>`,
};

displayStep("intro");
