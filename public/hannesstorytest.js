var steps = {};

steps["michel1803"] = {
	"mapID": 15832,
	"lat": 53.548490,
	"lon": 9.978966,
	"zoomlevel": 18,
	"description": "1803 war der Michel hier! <button onclick='displayStep(\"catha1829\")'>Weiter</button>",
};

steps["catha1829"] = {
	"mapID": 15841,
	"lat": 53.546023,
	"lon": 9.994222,
	"zoomlevel": 18,
	"description": "1829 war die Catharinenkirche echt schick oder? <button onclick='displayStep(\"catha1829b\")'>Weiter</button>",
};

steps["catha1829b"] = {
	"description": "TEST! Nur neuer Text. <button onclick='displayStep(\"catha1829c\")'>Weiter</button>",
};

steps["catha1829c"] = {
	"lat": 53.54,
	"lon": 9.99,
	"zoomlevel": 17,
	"description": "TEST2! Neue flyTo. <button onclick='displayStep(\"catha1829d\")'>Weiter</button>",
};

steps["catha1829d"] = {
	"mapID": 15735,
	"description": "TEST3! Neue Map.<button onclick='displayStep(\"stpauli1836\")'>Weiter</button>",
};

steps["stpauli1836"] = {
	"mapID": 15869,
	"lat": 53.546906,
	"lon": 9.957143,
	"zoomlevel": 18,
	"description": "Ausserhalb gab es die Kirche von St. Pauli, zB hier 1836. <button onclick='displayStep(\"michel1867\")'>Weiter</button>",
};

steps["michel1867"] = {
	"mapID": 15735,
	"lat": 53.548461,
	"lon": 9.978687,
	"zoomlevel": 14,
	"description": "Wer findet den Michel 1867? <button onclick='displayStep(\"diebsteich\")'>Weiter</button>",
};

steps["diebsteich"] = {
	"mapID": 15734,
	"lat": 53.565210,
	"lon": 9.942005,
	"zoomlevel": 18,
	"description": "Und hier ist der Diebsteich!",
};
