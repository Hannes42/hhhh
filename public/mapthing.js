var attribution = 'Karten und Bilder: CC-BY4.0: Staats- und Universitätsbibliothek Hamburg Carl von Ossietzky, ' + 
	'Map Background data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
	'Map Background Imagery © <a href="http://mapbox.com">Mapbox</a>';
var mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidzNidzNiIiwiYSI6ImNqMmMxdXdsMTAwMzAyeW8wanIwZmpueHEifQ.PO9rbg0zYGw2FExxo-Kd-A';

var maps = [
	{"id": 15832, "title": "Hamburg", "year": 1803},
	{"id": 15841, "title": "Grundriss Der Freien Stadt Hamburg / Entworfen 1819", "year": 1819},
	{"id": 15869, "title": "Grundriss der Vorstadt von Hamburg Sankt Pauli", "year": 1836},
	{"id": 15831, "title": "Grundriss von Hamburg", "year": 1836},
	{"id": 15845, "title": "Plan Des Brandes Vom 5. - 8. Mai", "year": 1842},
	{"id": 15840, "title": "Hamburg vor dem grossen Brande vom 5 - 8 Mai 1842", "year": 1842},
	{"id": 15877, "title": "Plan von Altona", "year": 1849},
	{"id": 15735, "title": "Grundriss von Hamburg und Umgebung", "year": 1863},
	{"id": 15842, "title": "Hamburg 1864", "year": 1864},
	{"id": 15829, "title": "Hamburgische Chronik von den ältesten Zeiten bis auf die Jetztzeit", "year": 1864},
	{"id": 15733, "title": "Plan von Hamburg, Altona und Umgegend 1867", "year": 1867},
	{"id": 15734, "title": "Hamburg, Altona, Wandsbek u. Umgebung 1929", "year": 1929},
];

var div_percent = 60/maps.length; // x% of the div
var percent = 10; // percent "start" value for first div

// generate the leaflet layers and push items to the timeline
var layers = [];
for (var map of maps) {
	document.getElementById('timeline').innerHTML += '<div class="timestamp" style="left: '+percent
	+'%;"><a class="timeline-link" onclick="switchToLayer('+map.id+');">'+map.year+'</a></div>';
	percent += div_percent;
	
	layers.push(L.tileLayer("https://mapwarper.net/maps/tile/"+map.id+"/{z}/{x}/{y}.png", {title: map.title, id: map.id, year: map.year}));
}
console.log(layers);

var streets = L.tileLayer(mbUrl, {id: 'mapbox.streets', attribution: attribution});

var activelayer = layers[0]; // so far only used to link the opacity slider to the active layer

var map = L.map('map', {
	center: [53.55, 10],
	zoom: 14,
	layers: [streets],
	zoomControl: false
});
map.attributionControl.setPrefix(""); // leaflet is known enough already ;)

var baseLayers = {
	"Aktuelle Hintergrundkarte": streets
};

var overlays = {};
// then add all the map layers
for (var layer of layers) {
	console.log(layer);
	overlays[layer.options.title] = layer; // http://stackoverflow.com/questions/10640159/key-for-javascript-dictionary-is-not-stored-as-value-but-as-variable-name
}

L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map);

var opacitySlider = new L.Control.opacitySlider({position: 'topleft'});
map.addControl(opacitySlider);

geomarker = L.geoJSON().addTo(map);

L.control.locate({
	strings: {
		title: "GPS-Position laden"
	},
	icon: "locatoriconcssclass",
	watch: true,
	enableHighAccuracy: true,
	layer: geomarker
}).addTo(map);

map.on('click', function(e) {
	document.getElementById('clickcoordinates').innerHTML = e.latlng.lat.toFixed(6) + ", " + e.latlng.lng.toFixed(6) + ", " + Math.floor(map.getZoom());
});

function getRandomNum(min, max) {
	return Math.random()*(max-min) + min;
}
	
function randomZoom() {
	lat = getRandomNum(53.55, 53.6);
	lon = getRandomNum(9.9, 10);
	zoom = getRandomNum(12, 16);
	flyToCoordinate(lat, lon, zoom);
}
	
function flyToCoordinate(lat, lon, zoom) {
	map.flyTo(new L.LatLng(lat, lon), zoom, {duration: 4}); // needs leaflet 1.x
}
	
function switchToLayer(layerid) {
	// find the layer with the id
	layer = layers.find(function (d) {
		return d.options.id === layerid;
	});

	map.addLayer(layer);
	map.removeLayer(activelayer);
	
	activelayer = layer;
	opacitySlider.setOpacityLayer(activelayer); // TODO update slider after layer switch, keep opacity per layer

	document.getElementById('timeline-currentmap').innerHTML = "→ <a href='http://mapwarper.net/maps/"+layer.options.id+"' target='_blank'>"+layer.options.title+" ("+layer.options.year+")</a>";
}

function displayStep(stepID) {
	step = steps[stepID];
	console.log(step);
	if (step['mapID']) {
		switchToLayer(step['mapID']);
		// TODO if only switch, no flyto -> slower!!!!1
	}
	if (step['lat']) {
		flyToCoordinate(step['lat'], step['lon'], step['zoomlevel']);
	}
	if (step['description']) {
		document.getElementById('sidepane').innerHTML = step['description'];
	}
}
