var steps = {};

// die knöpfe sind jeweils per hand im dict, damit man volle freiheit hat, was geht. man könnte ja auch branchen o.ä.
// die step namen können frei gewählt werden, sind hier einfach numerisch, aber alles geht
// text in backticks kann in javascript über mehrere zeilen gehen etc \o/

steps["intro"] = {
	"mapID": 15831,
	"lat": 53.553529,
	"lon": 9.992366,
	"zoomlevel": 15,
	"description": `<h3>Großer Brand 1842</h3>
	<a href="https://resolver.sub.uni-hamburg.de/goobi/PPN669598429" target="_blank">
	<img class='fittedimage' src='images/thumbnail/PPN669598429_00000001.cropped.jpg' />
	</a>
	<p>Hamburgs Großer Brand begann am 5. Mai 1842 in der Deichstraße. 
	Von dort aus breitete sich das Feuer, aufgrund der engen Bebauung, schnell in der Innenstadt und weiter Richtung Norden aus. 
	Bereits am Morgen des 6. Mai war die Nikolaikirche verloren. 
	Die Feuerwehr wollte durch gezielte Sprengungen Schneisen schaffen, durch die das Feuer sich nicht weiter ausbreiten kann. 
	Dieser Vorschlag wurde jedoch zunächst vom Senat abgelehnt.</p>
	<br />
	<div id='navigation'><button onclick='displayStep("2")'>→</button></div>`,
};

steps["2"] = {
	"lat": 53.548442,
	"lon": 9.992559,
	"zoomlevel": 16,
	"description": `Als das Feuer das alte Rathaus erreichte, stimmte der Senat für eine Sprengung des Rathauses. 
	Jedoch war dies ohne Erfolg. Das Feuer breitete sich weiter in Richtung Binnenalster aus. 
	Dort zerstörte es Gebäude in den großen Bleichen. 
	Erst die Binnenalster stoppte das Feuer im Norden, verhinderte jedoch nicht die Ausbreitung in Richtung Osten. 
	Dort fiel am 7. Mai die Petrikirche dem Feuer zum Opfer. 
	<br />
	<div id='navigation'>
	<button onclick='displayStep("intro")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("3")'>→</button>
	</div>`,
};

steps["3"] = {
	"lat": 53.552623,
	"lon": 9.999297,
	"zoomlevel": 16,
	"description": `Erst am 8. Mai wurde an der Straße Kurze Mühren die letzten Flammen gelöscht. 
	Die vier Tage Brand waren verheerend für Hamburg. 
	Mehr als 51 Menschen starben, fast 20.000 Menschen wurden obdachlos, ein Viertel der Stadt ist niedergebrannt. 
	Darunter mehr als 4.000 Wohnungen und Speicher, drei Kirchen, das Rathaus sowie das Stadtarchiv. 
	Jedoch war es eine Chance die Stadt neu aufzubauen und unter anderem das erste Kanalisationssystem zu errichten.
	<br />
	<div id='navigation'>
	<button onclick='displayStep("2")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("4")'>→</button>
	</div>`,
};

steps["4"] = {
	"mapID": 15829,
	"lat": 53.550125,
	"lon": 9.991990,
	"zoomlevel": 17,
	"description": `Nachdem am 6. Mai das Feuer das alte Rathaus erreichte, 
	bedrohte es auch die erst vor kurzem eröffnete Neue Börse, den Stolz aller Kaufleute. 
	Deshalb hat der Hamburger Kaufmann Theodor Dill, zusammen mit neun weiteren Freiwilligen, den Flammen den Kampf angesagt. 
	Sie räumten alles Brennbare nach draußen und löschten mit ihrem wenig zur Verfügung stehenden Wasser, 
	alle aufkommenden Brandherde. Nach 24 Stunden hissten sie eine weiße Fahne und das Gebäude war gerettet. 
	<a href="https://resolver.sub.uni-hamburg.de/goobi/PPN751404861" target="_blank">
	<img class='fittedimage' src='images/thumbnail/PPN751404861_00000001.cropped.jpg' />
	</a>
	<br />
	<div id='navigation'>
	<button onclick='displayStep("3")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("5")'>→</button>
	</div>`,
};

steps["5"] = {
	"mapID": 15840,
	"lat": 53.548009,
	"lon": 9.990177,
	"zoomlevel": 16,
	"description": `
	Die St. Nicolai Kirche fiel am 6. Mai den Flammen zum Opfer. 
	Sie war somit die erste Kirche, die durch das Feuer zerstörte wurde. 
	<a href="https://resolver.sub.uni-hamburg.de/goobi/PPN851737595" target="_blank">
	<img class='fittedimage' src='images/thumbnail/PPN851737595_00000001.cropped.jpg' />
	</a>
	<br />
	<div id='navigation'>
	<button onclick='displayStep("4")'>←</button>
	<button onclick='displayStep("intro")'>⌂</button>
	<button onclick='displayStep("6")'>→</button>
	</div>`,
};

steps["6"] = {
	"mapID": 15842,
	"lat": 53.551961,
	"lon": 9.994898,
	"zoomlevel": 15,
	"description": `Nachdem das Feuer am 8. Mai komplett gelöscht war, wurde das ganze Ausmaß der Schäden deutlich. 
	Vor allem die Innenstadt wies starke Schäden auf. 
	Die Binnenalster hatte dazu beigetragen, dass sich das Feuer nicht weiter Richtung Norden ausbreiten konnte, 
	wodurch schlimmeres verhinderte wurde.
	<a href="https://resolver.sub.uni-hamburg.de/goobi/PPN851827667" target="_blank">
	<img class='fittedimage' src='images/thumbnail/PPN851827667_00000001.cropped.jpg' />
	</a>
	<br />
	Quelle für den Text: <a href="http://www.ndr.de/kultur/geschichte/chronologie/1842-Der-Grosse-Brand-wuetet-in-Hamburg,grosserbrand101.html">NDR</a>
	<br />
	<div id='navigation'><button onclick='displayStep("intro")'>⌂</button></div>`,
};

displayStep("intro");
